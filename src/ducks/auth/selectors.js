export const visibilityState = (state, module) => {
  return state[module].toJS()['popupState'];
};

export const getUserName = (state, module) => {
  if(state[module].toJS()['user']) {
    return state[module].toJS()['user'].displayName;
  }
};

export const returnUserInfo = (user) => {
    if(user) {
      return {
        name: user.displayName,
        email: user.email,
        avatar: '',
        cinemaIDS: []
      };
    }
};