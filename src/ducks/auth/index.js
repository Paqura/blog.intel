import {
  SHOW_AUTH_POPUP,
  CLOSE_AUTH_POPUP,
  SIGN_UP_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_UP_FAILURE
} from './types';
import {SELECT_SAW_CINEMA_FAILURE, SELECT_SAW_CINEMA_SUCCESS} from "../cinemas/types";
import {Record} from 'immutable';

export const moduleName = 'auth';

const ReducerShema = Record({
  user: null,
  error: null,
  loading: false,
  popupState: false,
  cinemaIDS: []
});

export const reducer = (state = new ReducerShema(), action) => {
  const {type, payload, error} = action;
  switch (type) {
    case SIGN_UP_REQUEST:
      return state.set("loading", true);
    case SIGN_IN_SUCCESS:
      return state
        .set("loading", false)
        .set("user", payload)
        .set("error", null);
    case SIGN_UP_FAILURE:
      return state
        .set("loading", false)
        .set("error", error);
    case SHOW_AUTH_POPUP:
      return state
        .set('popupState', true);
    case CLOSE_AUTH_POPUP:
      return state
        .set('popupState', false);
    case SELECT_SAW_CINEMA_SUCCESS:
      return state
        .set('error', false);
    case SELECT_SAW_CINEMA_FAILURE:
      return state
        .set('error', error);
    default:
      return state;
  }
};