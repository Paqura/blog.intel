import {take} from 'redux-saga/effects';
import {SIGN_UP_REQUEST} from './types';
import {database} from 'firebase';
import {auth} from 'firebase';
import {returnUserInfo} from './selectors';

export const signUpSaga = function* () {
    while (true) {
      yield take(SIGN_UP_REQUEST);
      try {
        auth().onAuthStateChanged(function (user) {
          if (user) {
            const user = auth().currentUser;
            const payload = returnUserInfo(user);
            database().ref('users/' + user.uid).set(payload);

            window.location.href = '/';
          } else {
            console.log('error');
          }
        });
      }
      catch
        (err) {
        console.log(err);
      }
    }
  }
;