import {
  SHOW_AUTH_POPUP,
  CLOSE_AUTH_POPUP,
  SIGN_UP_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_IN_REQUEST,
  SHOW_NO_AUTH_POPUP,
  CLOSE_NO_AUTH_POPUP
} from './types';

import {auth} from 'firebase';

export const showAuthPopup = () => {
  return {type: SHOW_AUTH_POPUP}
};

export const closeAuthPopup = () => {
  return {type: CLOSE_AUTH_POPUP}
};

export const signUpStart = ({email, password, name}) => dispatch => {
  dispatch({type: SIGN_UP_REQUEST});


    auth()
    .createUserWithEmailAndPassword(email, password)
    .then(user => {
      user
        .updateProfile({displayName: name})
        .then(res => res)
        .catch(err => err);
      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: {
          user
        }
      });
    })
    .catch(err => dispatch({type: SIGN_UP_FAILURE, err}))
};

export const signInStart = () => async dispatch => {
  dispatch({type: SIGN_IN_REQUEST});


    auth()
    .onAuthStateChanged(function (user) {
      if (user) {
        dispatch({
          type: SIGN_IN_SUCCESS,
          payload: user
        });
      } else {
        dispatch({
          type: SIGN_UP_FAILURE,
          payload: 'err'
        });
      }
    });
};


export const showNoAuthPopup = () => dispatch => {
   dispatch({
     type: SHOW_NO_AUTH_POPUP
   })
};

export const closeNoAuthPopup = () => dispatch => {
  dispatch({
    type: CLOSE_NO_AUTH_POPUP
  })
};