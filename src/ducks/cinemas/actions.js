import {
  FETCH_CINEMA_START,
  FETCH_FINISHED_FILMS_START,
  SELECT_SAW_CINEMA_FAILURE,
  SELECT_SAW_CINEMA_SUCCESS,
  SELECT_SAW_CINEMA_START,
  FETCH_FINISHED_FILMS_SUCCESS,
  FETCH_FINISHED_FILMS_FAILURE,
  ADD_RATING_SUCCESS,
  ADD_RATING_START,
  ADD_RATING_FAILURE
} from './types';

import {database, auth} from 'firebase';

export const fetchCinemas = () => async dispatch => {
  dispatch({
    type: FETCH_CINEMA_START
  });

  const currentUser = auth().currentUser;
  if(currentUser) {
    dispatch({
      type: FETCH_FINISHED_FILMS_START
    });

    const ref = database().ref('users/' + currentUser.uid + '/cinemaIDS');
    const result = await ref.once('value').then(res => res.val());

    if(result) {
      dispatch({
        type: FETCH_FINISHED_FILMS_SUCCESS,
        payload: result
      })
    } else {
      dispatch({
        type: FETCH_FINISHED_FILMS_FAILURE,
        payload: 'error'
      })
    }    
  }
};

export const changeSawCinema = (id) => dispatch => {
  dispatch({
    type: SELECT_SAW_CINEMA_START
  });
  const currentUser = auth().currentUser;
  if(currentUser) {
    dispatch({
      type: SELECT_SAW_CINEMA_SUCCESS,
      payload: id
    });
    database().ref('users/' + currentUser.uid + '/cinemaIDS/' + id).set({ 'id': id });    
  } else {
    dispatch({
      type: SELECT_SAW_CINEMA_FAILURE,
      error: 'Для начала зарегистрируйся'
    });
  }
};

export const addRating = (id, value) => async dispatch => {
  dispatch({
    type: ADD_RATING_START
  });
  if(value) {    
    let payload = {
      id,
      value
    };
    dispatch({
      type: ADD_RATING_SUCCESS,
      payload
    });
    const currentUser = auth().currentUser;
    database().ref('users/' + currentUser.uid + '/cinemaIDS/' + id).set({ 'id': id, "rating": value });
  } else {
    dispatch({
      type: ADD_RATING_FAILURE
    })
  }
};