import {
  FETCH_FINISHED_FILMS_SUCCESS,
  FETCH_CINEMA_START,
  FETCH_CINEMA_SUCCESS,
  FETCH_FINISHED_FILMS_START,
  SELECT_SAW_CINEMA_START,
  SELECT_SAW_CINEMA_SUCCESS,
  SELECT_SAW_CINEMA_FAILURE,
  SHOW_POPUP_RATING_START,
  SHOW_POPUP_RATING_SUCCESS,
  SHOW_POPUP_RATING_FAILURE,
  ADD_RATING_SUCCESS
} from "./types";

import { addUidForCinemaItem }  from './selectors';
import { Map } from 'immutable';
import {finishedIDS as finishedSelectors, wrapCinemaRating} from './selectors';

export const moduleName = 'cinemas';

const initialState = new Map({
  cinemas: [],
  loading: null,
  uids: [],
  finishedCinemas: [],
  ratingID: null,
  ratingVisible: false,
  currentCinema: Map({})
});

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_CINEMA_START:
      return state
        .set('loading', true);
    case FETCH_CINEMA_SUCCESS:
      const payloadKeys = Object.keys(payload);
      const payloadToArray = addUidForCinemaItem(payload);
      return state
        .set('cinemas', payloadToArray)
        .set('uids', payloadKeys)
        .set('loading', false);

    case FETCH_FINISHED_FILMS_START:
       return state
         .set('loading', true);    
    case FETCH_FINISHED_FILMS_SUCCESS:
      const finishedID = finishedSelectors(payload);
      return state
        .set('finishedCinemas', finishedID)
        .set('loading', false);

    case SELECT_SAW_CINEMA_START:
      return state
          .set('loading', true);    
    case SELECT_SAW_CINEMA_SUCCESS:
      return state    
          .set('loading', false)
          .set('selectedCinemaID', true);
    case SELECT_SAW_CINEMA_FAILURE:
      return state    
        .set('loading', false);   

    case SHOW_POPUP_RATING_START:
      return state
        .set('ratingVisible', true)
        .set('loading', true);    
    case SHOW_POPUP_RATING_SUCCESS:
      return state
        .set('loading', false)
        .set('ratingID', payload);    
    case SHOW_POPUP_RATING_FAILURE:
      return state
        .set('loading', false);
           
    case ADD_RATING_SUCCESS:
      const itemWithID = wrapCinemaRating(payload);
      let prevState = state.toJS().currentCinema;
      let merged = Object.assign({}, prevState, itemWithID);
      return state
        .set('currentCinema', merged)
        .set('ratingVisible', false);    
    default:
      return state;
  }
};