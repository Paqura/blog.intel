import {moduleName} from './index';

export const stateSelector = function(state) {
  const selector = state[moduleName].get('cinemas');  
  return selector;
};

export const addUidForCinemaItem = obj => {
  const payloadToArray = Object.values(obj);
  payloadToArray.forEach( (it, i) => {
    it['uid'] = Object.keys(obj)[i];    
  }); 
  return payloadToArray;
};

export const finishedIDS = obj => {
   const result = Object.values(obj);
   return result;
};

export const finishedCinemasID = state => {
   return state[moduleName].get('finishedCinemas');
};

export const wrapCinemaRating = (payload) => {
  const name = Object.values(payload)[0];
  return {
    [name]: payload
  }
};



