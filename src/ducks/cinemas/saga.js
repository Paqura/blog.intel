import {put, take, call} from 'redux-saga/effects';
import {
  FETCH_CINEMA_FAILURE,
  FETCH_CINEMA_START,
  FETCH_CINEMA_SUCCESS,
  SELECT_SAW_CINEMA_SUCCESS,
  SHOW_POPUP_RATING_START,
  SHOW_POPUP_RATING_SUCCESS,
  SHOW_POPUP_RATING_FAILURE
} from "./types";

import {database} from 'firebase';

export const fetchCinemaSaga = function* () {
  while (true) {
    yield take(FETCH_CINEMA_START);
    try {
      const ref = database().ref('cinemas');
      const cinemas = yield call([ref, ref.once], 'value');

      yield put({
        type: FETCH_CINEMA_SUCCESS,
        payload: cinemas.val()
      })
    } catch (err) {
      yield put({type: FETCH_CINEMA_FAILURE, payload: err})
    }
  }
};

export const showRatingPopup = function* () {
  while(true) {
    const action = yield take(SELECT_SAW_CINEMA_SUCCESS);
    yield put({
      type: SHOW_POPUP_RATING_START
    })
    if (action.payload) {
      yield put({
        type: SHOW_POPUP_RATING_SUCCESS,
        payload: action.payload
     })
    } else {
      yield put({
        type: SHOW_POPUP_RATING_FAILURE,
        error: `action payload: ${action.payload}`
     })
    }  
  }
}




