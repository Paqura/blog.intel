import {put, take, call} from 'redux-saga/effects';
import {
  FETCH_LIBRARY_REQUEST,
  FETCH_LIBRARY_SUCCESS,
  FETCH_LIBRARY_FAILURE,
  ADD_BOOKMARK_REQUEST,
  ADD_BOOKMARK_SUCCESS,
  ADD_BOOKMARK_FAILURE,
  FETCH_BOOKMARK_SUCCESS,
  FETCH_BOOKMARK_FAILURE
} from './types';
import {database, auth} from 'firebase';

export const fetchLibrarySaga = function* () {
  while (true) {
    yield take(FETCH_LIBRARY_REQUEST);

    try {
      const ref = database().ref('library');
      const libraryList = yield call([ref, ref.once], 'value');

      yield put({
        type: FETCH_LIBRARY_SUCCESS,
        payload: libraryList.val()
      })
    } catch (err) {
      yield put({
        type: FETCH_LIBRARY_FAILURE,
        payload: err
      })
    }
  }
};

export const addToBookmars = function* () {
  while (true) {
    let action = yield take(ADD_BOOKMARK_REQUEST);
    try {
      const currentUser = auth().currentUser;
      const ref = database().ref('/users/' + currentUser.uid + '/bookmarks/' + action.payload);
      yield call([ref, ref.set], {"id": action.payload});
      yield put({
        type: ADD_BOOKMARK_SUCCESS
      });
    } catch (err) {
      yield put({
        type: ADD_BOOKMARK_FAILURE,
        err
      })
    }
  }
};

export const fetchBookmarks = function* () {
  while (true) {
    yield take(FETCH_LIBRARY_SUCCESS);
    try {
      const currentUser = auth().currentUser;
      const ref = database().ref('/users/' + currentUser.uid + '/bookmarks');
      const bookmarks = yield call([ref, ref.once], 'value');

      yield put({
        type: FETCH_BOOKMARK_SUCCESS,
        payload: bookmarks.val()
      })
    } catch (err) {
      yield put({
        type: FETCH_BOOKMARK_FAILURE
      })
    }
  }
};

