import {
  FETCH_LIBRARY_REQUEST,
  ADD_BOOKMARK_REQUEST
} from './types';

export const fetchLibrary = () => dispatch => {
  dispatch({
    type: FETCH_LIBRARY_REQUEST
  })
};

export const addBookMark = (id) => dispatch => {
  dispatch({
    type: ADD_BOOKMARK_REQUEST,
    payload: id
  })
};
