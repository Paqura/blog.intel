import { moduleName } from './index';

export const getLibrary = state => {
  return state[moduleName].get('library')
}