import { Map } from "immutable";
import {
  ADD_BOOKMARK_REQUEST, ADD_BOOKMARK_SUCCESS, FETCH_BOOKMARK_SUCCESS, FETCH_LIBRARY_REQUEST,
  FETCH_LIBRARY_SUCCESS
} from "./types";
import {addUidForCinemaItem} from '../cinemas/selectors';

export const moduleName = "library";

const initialState = new Map({
  library: [],
  bookmarks: [],
  loading: false
});

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_LIBRARY_REQUEST:
      return state
        .set("loading", true);
    case FETCH_LIBRARY_SUCCESS:
      const libs = addUidForCinemaItem(payload);
      return state
        .set("library", libs)
        .set("loading", false);
    case ADD_BOOKMARK_REQUEST:
      return state
        .set('loading', true);
    case ADD_BOOKMARK_SUCCESS:
      return state
        .set('loading', false);
    case FETCH_BOOKMARK_SUCCESS:
      const preState = [...state.get('bookmarks'), payload];
      const bookmarks = Object.keys(preState[0]);
      return state
        .set('bookmarks', bookmarks)
        .set('loading', false);
    default:
      return state;
  }
};
