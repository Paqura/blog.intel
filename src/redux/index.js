import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer';
import logger from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import history from '../history';
import createSagaMiddleware  from 'redux-saga';
import { saga as rootSaga } from './saga';
import thunk from 'redux-thunk';

const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(
  sagaMiddleware, routerMiddleware(history), 
  logger,
  thunk
);
const store = createStore(reducer, enhancer);

sagaMiddleware.run(rootSaga);

export default store;