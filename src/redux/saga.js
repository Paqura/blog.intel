import { all } from 'redux-saga/effects';
import {fetchCinemaSaga, showRatingPopup} from '../ducks/cinemas/saga';
import {fetchLibrarySaga, addToBookmars, fetchBookmarks} from '../ducks/library/saga';
import {signUpSaga} from '../ducks/auth/saga';

export const saga = function * () {
  yield all([
    fetchCinemaSaga(),
    fetchLibrarySaga(),
    fetchBookmarks(),
    addToBookmars(),
    showRatingPopup(),
    signUpSaga()
  ])
};
