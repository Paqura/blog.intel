import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as cinemaReducer, moduleName as cinemas } from '../ducks/cinemas';
import { reducer as form } from 'redux-form';
import { reducer as authReducer, moduleName as auth} from "../ducks/auth";
import { reducer as libraryReducer, moduleName as library } from '../ducks/library';

export default combineReducers({
  router,
  form,
  [cinemas]: cinemaReducer,
  [auth]: authReducer,
  [library]: libraryReducer
})