import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import ButtonSend from './ButtonSend';

const styles = () => ({
  container: {
    width: '100%'
  },
  textField: {
    width: '100%'
  }
});

class SignIn extends Component {
  render() {
    const {classes} = this.props;
    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="email-input"
          label="Email"
          className={classes.textField}
          type="email"
          margin="normal"
        />
        <TextField
          id="password-input"
          label="Пароль"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
        />
        <ButtonSend />
      </form>
    );
  }
}


export default withStyles(styles)(SignIn);