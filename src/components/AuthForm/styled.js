import styled from 'styled-components';

export const CommonOverlay = styled.div`
   position: fixed;
   left: 0;
   top: 0;
   width: 100vw;
   height: 100vh;
   background-color: rgba(0, 0, 0, 0.24);
   z-index: 99;
`;

export const Wrapper = {
  maxWidth: '400px',
  width: '100%',
  margin: '0 auto',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  zIndex: '100'
};

export const FormHeader = styled.div`
   padding: 16px;   
`;