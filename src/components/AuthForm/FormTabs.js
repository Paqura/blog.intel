import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Tabs, {Tab} from 'material-ui/Tabs';
import {FormHeader} from './styled';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

class FormTabs extends Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({value});
  };

  checkValForm = (values) => {
    this.props.signUpStart(values);
  };


  render() {
    const {classes} = this.props;
    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="Войти"/>
          <Tab label="Регистрация"/>
        </Tabs>
        <FormHeader>
          {!this.state.value ? <SignIn /> : <SignUp onSubmit={this.checkValForm}/>}
        </FormHeader>
      </Paper>
    );
  }
}

export default withStyles(styles)(FormTabs);
