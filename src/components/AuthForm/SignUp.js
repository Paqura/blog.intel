import React, {PureComponent} from 'react';
import {withStyles} from 'material-ui/styles';
import ButtonSend from './ButtonSend';
import { Field, reduxForm } from 'redux-form'
import {TextField} from 'redux-form-material-ui'

const styles = () => ({
  container: {
    width: '100%'
  },
  textField: {
    width: '100%',
    marginTop: '4px'
  }
});

class SignUp extends PureComponent {
  render() {
    const {classes, handleSubmit} = this.props;
    return (
      <form
        onSubmit={handleSubmit}
        className={classes.container}
        noValidate autoComplete="off"
      >
        <Field
          label="Имя"
          name="name"
          type="text"
          component={TextField}
          className={classes.textField}
        />
        <Field
          label="Email"
          name="email"
          type="email"
          component={TextField}
          className={classes.textField}
        />
        <Field
          label="Пароль"
          name="password"
          type="password"
          component={TextField}
          className={classes.textField}
        />
        <ButtonSend />
      </form>
    );
  }
}

export default withStyles(styles)(reduxForm({
  form: 'singUp'
})(SignUp));