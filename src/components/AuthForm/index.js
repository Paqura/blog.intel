import React, {Component, Fragment} from 'react';
import FormTabs from './FormTabs';
import {connect} from 'react-redux';
import {closeAuthPopup, signUpStart} from '../../ducks/auth/actions';
import {moduleName} from '../../ducks/auth';
import Paper from 'material-ui/Paper';
import {visibilityState} from '../../ducks/auth/selectors';
import {
  Wrapper,
  CommonOverlay
} from './styled';


class AuthForm extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props.visibility !== nextProps.visibility;
  }
  render() {
    const {visibility, closeAuthPopup, signUpStart} = this.props;
    return (
      visibility
        ? <Fragment>
          <CommonOverlay onClick={closeAuthPopup}/>
          <Paper style={Wrapper}>
            <FormTabs signUpStart={signUpStart}/>
          </Paper>
        </Fragment>
        : null
    )
  }
}

const mapStateToProps = state => ({
  visibility: visibilityState(state, moduleName)
});

const mapDispatchToProps = ({
  closeAuthPopup,
  signUpStart
});

export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(AuthForm);