import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  button: {
    margin: '32px 0 0 0',
    width: '100%'
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  }
});

function ButtonSend(props) {
  const { classes } = props;
  return (
      <Button
        className={classes.button}
        variant="raised"
        color="primary"
        type='submit'
      >
        Войти
      </Button>

  );
}

export default withStyles(styles)(ButtonSend);