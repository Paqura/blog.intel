import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import ApplicationBar from './AppBar';
import Welcome from './routes/Welcome';
import Profile from './routes/Profile';
import Library from './routes/Library';
import Cinema from './routes/Cinema';
import Poems from './routes/Poems';
import AuthForm from '../components/AuthForm';
import {signInStart} from '../ducks/auth/actions';
import {moduleName} from '../ducks/auth';
import {getUserName} from '../ducks/auth/selectors';
import NoAuthError from '../components/Error/NoAuthError';
import ScrollToTop from '../components/ScrollToTop';


class Root extends Component {
   componentDidMount() {     
    this.props.signInStart();
  }

  render() {
    const { userName, error } = this.props;
   console.log('root rerender')
    return (
      <div className='app'>
        {error && <NoAuthError error={error}/>}
        <ApplicationBar userName={userName}/>
        <AuthForm/>
        <ScrollToTop />
        <Switch>
          <Route exact path='/' component={Welcome}/>
          <Route exact path='/profile' component={Profile}/>
          <Route exact path='/library' component={Library}/>
          <Route exact path='/cinema' component={Cinema}/>
          <Route exact path='/poems' component={Poems}/>
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userName: getUserName(state, moduleName),
  error: state[moduleName].toJS().error
});

const mapDispatchToProps = ({
  signInStart
});

export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(Root);