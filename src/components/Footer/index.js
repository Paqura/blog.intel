import React from 'react';
import {Copyright, FooterWrapper} from './styles';

function FooterContent(props) {
  return (
    <FooterWrapper>
      <nav className='wrapper'>
        <Copyright>Для тех, у кого плохая память: сейчас 2018 год</Copyright>
      </nav>
    </FooterWrapper>
  )
}

export default FooterContent;