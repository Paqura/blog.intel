import styled from 'styled-components';
import {blueGrey} from "material-ui/colors/index";

export const FooterWrapper = styled.footer`
  position: absolute;
  bottom: 0;
  left: 0;
  height: auto;
  background-color: ${blueGrey[800]};
  width: 100vw;
  overflow: hidden;
`;

export const FooterList = styled.ul`
   list-style-type: none;
   padding: 0;
   margin: 0;
   display: flex;
`;

export const Copyright = styled.p`
   display: flex;
   justify-content: center;
   color: #939aaa;
   font-size: 14px;
`;