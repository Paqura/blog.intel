//@flow

import React from 'react';
import {List} from '../../components/Welcome/styles';
import CinemaItem from './CinemaItem';
import DLoader from '../Loader';
import unId from 'uniqid';
import pure from 'recompose/pure';

type Cinema = {
  title: string,
  text: string,
  img: string,
  genre: string,
  type: string,
  rating: string,
  url: string,
  uid: string,
  addRating?: Function
};

type Props = {
  loading: boolean,
  cinemas: Array<Cinema>,
  changeSawCinema: Function,
  status: boolean,
  finishedCinemas: any,
  rating?: boolean,
  addRating: Function,
  ratingVisible?: any,
  currentCinema?: any
};

const renderCinemas = (cinema,  changeSawCinema, finishedCinemas, rating, addRating, ratingVisible, currentCinema) => {
  const ID : string = unId();
  const cinemaType : string = cinema.type.toLowerCase(); // Кино или Сериал
  const coincidence = finishedCinemas.filter(it => it.id === cinema.uid).length; // совпадение (есть ли фильм в просмотренных)
  const customRating = finishedCinemas.filter(it => it.id === cinema.uid).map(it => it.rating)[0]; // рейтинг юзера

  return (
    <CinemaItem
      key={unId()}
      ID={ID}
      cinema={cinema}
      cinemaType={cinemaType}
      coincidence={coincidence}
      customRating={customRating}
      rating={rating}
      addRating={addRating}
      ratingVisible={ratingVisible}
      currentCinema={currentCinema}
      changeSawCinema={changeSawCinema}
    />
  )
};

function CinemaList(props : Props) {
  const {
    rating,
    addRating,
    ratingVisible,
    currentCinema,
    loading,
    cinemas,
    changeSawCinema,
    finishedCinemas
  } = props;

  return (
    !loading
    ?
      <List className="cinema-list" key={unId()}>
          {cinemas
            .map(it => renderCinemas(
              it,
              changeSawCinema,
              finishedCinemas,
              rating,
              addRating, 
              ratingVisible,
              currentCinema
            ))
          }
       </List>     
    : 
    <DLoader/>
  )
}

export default pure(CinemaList);