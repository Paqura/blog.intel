import React from 'react';
import {CinemaType} from './styles';

export default function CinemaTypeComponent({cinemaType}) {
  return (
    <CinemaType>{cinemaType}</CinemaType>
  )
}