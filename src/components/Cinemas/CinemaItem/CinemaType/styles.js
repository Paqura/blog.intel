import styled from 'styled-components';

export const CinemaType = styled.span` 
  position: absolute;
  top: 0;
  left: 0;
  font-size: 14px;
  display: inline-block;
  margin-top: 12px;
  background-color: #000;
  color: #fff;
  padding: 8px 16px;
`;