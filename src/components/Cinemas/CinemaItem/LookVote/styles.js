import styled from 'styled-components';

export const RatingCount = styled.span`
   font-size: 14px;
   margin-left: 8px;
   text-decoration: underline;
   font-weight: bold;
`;

export const LookRatingWrapper = styled.div`
  display: flex;
  align-items: baseline;  
`;

export const ChangeRatingBtnWrapper = {
  marginTop: '16px'
};

export const SelectViewText = styled.h4`
  margin: 0;
`;