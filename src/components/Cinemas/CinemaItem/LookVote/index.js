import React, { Fragment } from 'react';
import { 
  SelectViewText, 
  LookRatingWrapper, 
  RatingCount, 
  ChangeRatingBtnWrapper 
} from './styles';
import Button from 'material-ui/Button';

function LookRating({customRating}) {
  return (
    <Fragment>
      <LookRatingWrapper>
        <SelectViewText>Ваша оценка:</SelectViewText>
        <RatingCount>{customRating}</RatingCount>
      </LookRatingWrapper>
      <Button
        style={ChangeRatingBtnWrapper}
        color="primary"
        variant="raised"
        disabled
      >
        Изменить
      </Button>
    </Fragment>
  )
}

export default LookRating;
