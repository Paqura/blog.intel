import React from 'react';
import CinemaImage from './CinemaImage';
import SelectViewButton from './SelectViewButton';
import GenreComponent from './Genre';
import {CardContent} from 'material-ui/Card';
import CinemaTypeComponent from './CinemaType';
import ReadMore from './ReadMore';
import Paper from 'material-ui/Paper';
import {ItemHeader, Item, LinkStyle} from '../../Welcome/styles';
import MyRating from './MyRating';
import pure from 'recompose/pure'

const CardContentStyle = {
   width: '100%',
   padding: "16px"
};

function CinemaItem(
  {
    ID,
    cinema,
    cinemaType,
    coincidence,
    customRating,
    ratingVisible,
    changeSawCinema,
    rating,
    addRating,
    currentCinema
  }
  ) {
  return (
    <Paper key={ID} className="hovering">
      <Item style={LinkStyle}>
        <CinemaImage image={cinema.img}/>
        <CardContent style={CardContentStyle}>
          <ItemHeader>{cinema.title}</ItemHeader>
          <CinemaTypeComponent cinemaType={cinemaType} />
          <GenreComponent genre={cinema.genre}/>
          <MyRating rating={cinema.rating}/>
          <ReadMore url={cinema.url} id={ID}/>
          <SelectViewButton
            ratingVisible={ratingVisible}
            changeSawCinema={changeSawCinema}
            id={cinema.uid}
            coincidence={coincidence}
            link={cinema.link}
            rating={rating}
            addRating={addRating}
            customRating={customRating}
            currentCinema={currentCinema}
          />
        </CardContent>
      </Item>
    </Paper>
  )
}

export default pure(CinemaItem);