//@flow

import React, {Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import {SelectViewText} from './styles';
import LookVote from '../LookVote';
import OwnRating from '../OwnRating';
import {Link} from 'react-router-dom';
import Tooltip from 'material-ui/Tooltip';


const styles = () => ({
  button: {
    margin: '16px 0 0 0',
    letterSpacing: '1px',
    fontSize: '14px',
    display: 'block',
    textTransform: 'none',
    padding: '0',
    marginRight: 'auto'   
  },
  buttonWrapper: {
    marginTop: '22px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: '#efefef7a',
    padding: '16px',
    borderRadius: '4px'
  },
  buttonFlex: {
    display: 'flex'
  },
  link: {
    position: 'relative',
    margin: '0 0 0 8px',
    textDecoration: 'none',
    color: '#172a3fb8',
    display: 'flex',
    alignItems: 'center',
    fontFamily: 'Roboto Slab', 
    fontSize: '14px',  
    padding: '4px 16px',
    opacity: '0.7',
    transition: '400ms',
    '&:hover': {      
        opacity: '1'
    }
  },
  tooltip: {
    marginTop: '16px',
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px'
  }
});

type Props = {
  classes: {
    button: string,
    buttonWrapper: string,
    buttonFlex: string
  },
  changeSawCinema: Function,
  id: string,
  status: boolean,
  rating?: boolean,
  addRating: Function
};

function SelectViewButton(props: Props) {
  const {classes, changeSawCinema, id, coincidence, rating, addRating, customRating, ratingVisible, currentCinema, link} = props;
  const flag = rating === id;

  const selectedEqual = currentCinema[id] ? currentCinema[id].id === id : false;

  return (
    <div className={classes.buttonWrapper}>
      {coincidence
        ?
        <LookVote customRating={customRating}/>
        :
        flag && ratingVisible
          ?
          <OwnRating addRating={addRating} id={id}/>
          :
          selectedEqual && currentCinema ?
            <LookVote customRating={currentCinema[id].value}/>
            : <Fragment>
              <SelectViewText>Смотрел(а)?</SelectViewText>
              <div className={classes.buttonFlex}>
                <Button
                  size='medium'
                  color="primary"
                  variant="raised"
                  style={{marginRight: '8px'}}
                  className={classes.button}
                  onClick={changeSawCinema.bind(null, id)}
                >
                  Да
                </Button>  
                <Tooltip id="tooltip-top" title="Переход на сайт с фильмом" placement="top" className={classes.tooltip}>
                   <Link className={classes.link} to={link} target='_blank'>К фильму</Link >
                </Tooltip>          
                 
              </div>
            </Fragment>
      }
    </div>
  );
}

export default withStyles(styles)(SelectViewButton);