import React from 'react';
import {Genre, GenreList} from './styles';

const renderGenres = (genre) => {
  return (
    <Genre key={genre.toString()}>
      {genre}
    </Genre>
  )
};

export default function GenreComponent(props) {
  const genreList = props.genre.toLowerCase().split(' ');
  return (
      <GenreList> {genreList.map(it => renderGenres(it))} </GenreList>
  )
}