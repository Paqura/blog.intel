import styled from 'styled-components';

export const GenreList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  padding: 0;
  margin: 16px 0 0 0;
  list-style-type: none;
`;

export const Genre = styled.li`  
  cursor: default;
  padding: 4px 8px;
  background-color: #ebebff;
  border-radius: 4px;
  margin-left: 8px;
  font-size: 14px;
  font-weight: 600;
  color: #4e4e4e;
  &:first-child {
    margin-left: 0;
  }
  &:hover {
    background-color: #f0f0f0;
  }
`;