import React from 'react';
import {Rating} from 'material-ui-rating';
import {SelectViewText, RatingWrapper, OverlayRating} from './styles';

export default function OwnRating(props) {
  const handleChange = (id, value) => {
    props.addRating(id, value);
  };
  return (
    <OverlayRating>
      <RatingWrapper>
        <SelectViewText>Поставь свою оценку</SelectViewText>
        <Rating
          value={6}
          max={10}
          onChange={(value) => handleChange(props.id, value)}
        />
      </RatingWrapper>
    </OverlayRating>
  )
}