import styled from 'styled-components';

export const RatingWrapper = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  transition: 400ms;
  z-index: 1000;
  margin-top: 22px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  background-color: #ffffff;
  padding: 16px;
  border-radius: 4px;
  @media (max-width: 480px) {
    left: 4px;
    width: calc(100vw - 8px);
    transform: translate(0);
    & button {
      width: 26px;
      height: 26px;
      margin-top: 8px;
    }
  }  
`;

export const OverlayRating = styled.div`
   position: fixed;
   left: 0;
   top: 0;
   width: 100%;
   height: 100%;
   z-index: 1000;
   background-color: rgba(0, 0, 0, 0.2);   
`;

export const SelectViewText = styled.h4`
  margin: 0;
`;