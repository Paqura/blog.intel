import styled from "styled-components";

export const CinemaRating = styled.span`
  display: flex;
  margin-top: 12px;
  color: #172a3fb8;
  font-size: 14px;
`;