import React from 'react';
import {CinemaRating} from './styles';

export default function MyRating(props) {
  return (
    <CinemaRating> Моя оценка: {props.rating} из 10</CinemaRating>
  )
}