//@flow

import React from 'react';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import {Link} from 'react-router-dom';
import Rowing from 'material-ui-icons/Rowing';

const styles = theme => ({
  button: {
    margin: '16px 0 0 0',
    letterSpacing: '1px',
    fontSize: '14px',
    display: 'flex',
    textTransform: 'none',
    color: "#212121",
    backgroundColor: "#fff",
    "&:hover": {
      backgroundColor: "#424242",
      color: "#ffffff"
    }
  },
  sendIcon: {
    marginLeft: '4px'
  },
});

const LinkStyle = {
  color: 'currentColor',
  textDecoration: 'none'
};

type Props = {
  classes: {
    button: string,
    sendIcon: string
  },
  url: string,
};

function ReadMore(props: Props) {
  const {classes, url} = props;
  return (
    <Button size='medium' className="hovering" style={{ marginTop: '8px' }}>
      <Link style={LinkStyle} to={url} target="_blank">На кинопоиск</Link>
      <Rowing className={classes.sendIcon}/>
    </Button>
  );
}

export default withStyles(styles)(ReadMore);