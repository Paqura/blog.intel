import React from 'react';
import PlaceholderComponent from './PlaceholderComponent';
import {CardMedia} from 'material-ui/Card';
import LazyLoad from 'react-lazyload';

export const CinemaImg = {
  width: '100%',
  height: '300px',
  backgroundSize: 'contain'
};

export default function CinemaImage(props) {

  const {image} = props;
  return (
    <LazyLoad 
      height={240}
      throttle={20}
      placeholder={< PlaceholderComponent />}
    >
      <CardMedia style={CinemaImg} image={image} title="Cinema poster"/>
    </LazyLoad>
  )
}