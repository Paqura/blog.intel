import React from 'react';
import styled from 'styled-components';

const LoadingWrapper = styled.div`
   display: flex;
   justify-content: center;
   align-items: center;
   width: 100%;
   height: 300px;
   filter: blur(2px);
   background-color: #ECEFF1;
`;

function PlaceholderComponent () {
  return(
      <LoadingWrapper/>
    );
}

export default PlaceholderComponent;