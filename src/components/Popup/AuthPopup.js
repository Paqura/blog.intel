import React, {Component} from 'react';
import { auth } from 'firebase';

class AuthPopup extends Component  {
  constructor(props) {
    super(props);
    this.txtEmail = null;
    this.txtPassword = null;

    this.setEmailRef = element => {
      this.txtEmail = element;
    };

    this.setPasswordRef = element => {
      this.txtPassword = element;
    };
  }

  shouldComponentUpdate(nextProps) {
    return nextProps
  }

  const authWithEmail = (evt) => {
    evt.preventDefault();
    const email = this.txtEmail.value;
    const pass = this.txtPassword.value;
    const promise = auth().createUserWithEmailAndPassword(email, pass);
    promise
       .then(result => result)
       .catch(err => console.error(err));
  }

  const checkPos = (evt) => {
    evt.preventDefault();
    const user = auth().currentUser;
    user.updateProfile({
      displayName: "Jane Q. User",
      photoURL: "https://example.com/jane-q-user/profile.jpg"
    }).then(function() {

    }).catch(function(error) {

    });
  }
 
  render() {
    return (
      <form>
        <input 
          type="email" 
          ref={ this.setEmailRef }
        />
        <input 
          type="password" 
          ref={ this.setPasswordRef }
        />
        <button onClick={this.checkPos}>Click</button>
      </form>
    )
  }
}

export default AuthPopup;