export const Scroller = {
  zIndex: '999',
  backgroundColor: '#ffffff',
  padding: '16px',
  left: '50%',
  top: '80px',
  border: '10px solid #37474f',
  transform: 'translateX(-50%)',
  bottom: 'auto',
  boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.03), 8px 14px 38px rgba(39,44,49,.06), 1px 3px 8px rgba(39,44,49,.03)',
  borderRadius: '50%',
  width: '80px',
  height: '80px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  transition: '400ms linear'
};