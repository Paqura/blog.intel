import React, {Component} from 'react';
import ScrollUp from 'react-scroll-up';
import {Scroller as ScrollStyle} from './styles';
import debounce from 'lodash.debounce';
import AirplanemodeActive from 'material-ui-icons/AirplanemodeActive';

export default class ScrollToTop extends Component {
  state = {
    axis: false
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.axis !== this.state.axis) {
      return true;
    }
    return false;
  }

  componentDidMount() {
    window.addEventListener('wheel', debounce(this.checkDeltaY, 20));
  }

  componentWillUnmount() {
    window.removeEventListener(this.checkDeltaY);
  }

  checkDeltaY = evt => {
    if(evt) {
      let {deltaY} = evt;
      if(deltaY > 0) this.setState({ axis: false });
      if(deltaY < 0) this.setState({ axis: true });
    }
  };

  render() {
    return (
        this.state.axis
        ?
        <ScrollUp
          showUnder={320}
          style={ScrollStyle}
        >
          <AirplanemodeActive/>
        </ScrollUp>
        :
        null
    )
  }
}