import React from 'react';
import Book from './Book';
import { List } from '../Welcome/styles';

const renderBook = (book, addBookMark, bookmarks) => {
  return <Book book={book} key={book.uid} addBookMark={addBookMark} bookmarks={bookmarks}/>
};

export default function LibraryList(props) {
  const {library, addBookMark, bookmarks} = props;
  return (
    <List>
      { library.map(book => renderBook(book, addBookMark, bookmarks)) }
    </List>
  )
}