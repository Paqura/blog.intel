import React from 'react';
import Button from 'material-ui/Button';
import { TouchApp } from 'material-ui-icons';
import Dialog, { DialogContent, DialogContentText,  withMobileDialog} from 'material-ui/Dialog';
import {ReadMoreStyle, CloseWindow} from './styles';

class FullDescription extends React.Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const {fullScreen} = this.props;
    return (
      <div>
        <Button style={ReadMoreStyle} onClick={this.handleClickOpen}>Читать больше</Button>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title">
         <Button style={CloseWindow} onClick={this.handleClose}><TouchApp/></Button>
          <DialogContent>
            <DialogContentText>
            {this.props.story}
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default withMobileDialog('p')(FullDescription);
