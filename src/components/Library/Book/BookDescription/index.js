import React, { Fragment } from 'react';
import {Description} from './styles';
import FullDescription from '../FullDescription';


const renderSmallText = text => {
  const MAX_SYMBOLS = 50;
  if(text.length > MAX_SYMBOLS) {
    return text.split('').filter((it, i) => i < MAX_SYMBOLS).join('') + '...';
  }
  return text;
};

export default function BookDescription(props) {
  return (
    <Fragment>
      <Description>
        {renderSmallText(props.description)}     
      </Description>  
      <FullDescription story={props.story}/>
    </Fragment>
  )
}