import styled from 'styled-components';

export const Description = styled.p`
    margin: 8px 0;
    font-size: 14px;
`;

