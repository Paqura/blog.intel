import React from 'react';
import BookPicture from './BookPicture';
import BookAuthor from './BookAuthor';
import BookYear from './BookYear';
import BookDescription from './BookDescription';
import BookMark from './BookMark';
import styled from 'styled-components';

const BookWrapper = styled.div`
   display: flex;   
   flex-direction: column;
   padding: 16px;  
   position: relative;
`;

export default function Book(props) {
  const {author, year, description, story, picture, uid} = props.book;
  const { addBookMark, bookmarks } = props;
  const flagToBookmark = bookmarks.filter(it => it === uid).length;
  return (
      <BookWrapper className='hovering'>
        <BookMark addBookMark={addBookMark} uid={uid} sameID={flagToBookmark}/>
        <BookPicture picture={picture}/>
        <BookAuthor author={author}/>
        <BookYear year={year}/>
        <BookDescription description={description} story={story}/>
      </BookWrapper>
  )
}