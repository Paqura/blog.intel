import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import Bookmark from 'material-ui-icons/Bookmark';

const styles = theme => ({
  button: {
    margin: 0,
    padding: 0,
    position: 'absolute',
    right: '16px',
    color: '#fff',
    minWidth: '40px'
  },
  icon: {
    fill: '#546e7a'
  },
  iconFilled: {
    fill: '#7a352d'
  }
});

class BookMark extends Component {
  handleClick = () => {
    this.props.addBookMark(this.props.uid);
  };
  render() {
    const {classes, sameID} = this.props;
    return (
      <Button mini aria-label="add" className={classes.button} onClick={this.handleClick}>
        <Bookmark className={sameID ? classes.iconFilled : classes.icon}/>
      </Button>
    );
  }

}

export default withStyles(styles)(BookMark);
