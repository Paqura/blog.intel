import React from 'react';
import PlaceholderComponent from './PlaceholderElement';
import {CardMedia} from 'material-ui/Card';
import LazyLoad from 'react-lazyload';
import {MediaStyle} from './styles';

function BookPicture(props) {
  return (
    <LazyLoad 
       height={240} 
       debounce={200} 
       placeholder={< PlaceholderComponent />}>
      <CardMedia 
        style={MediaStyle} 
        image={props.picture}
        title="Cinema poster"
      />
    </LazyLoad>
  )
}

export default BookPicture;
