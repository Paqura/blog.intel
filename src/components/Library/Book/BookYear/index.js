import React from 'react';
import {Born} from './styles';

export default function BookYear(props) {
  return (
     <Born>Напечатана в <b>{props.year}</b> году</Born>
  )
}