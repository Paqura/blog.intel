import styled from 'styled-components';

export const Born = styled.span`
  display: block;
  color: #172a3fb8;
  font-size: 14px;
  text-align: center;
  background-color: #efefef7a;
  padding: 8px 0;
`;