import styled from 'styled-components';

export const Author = styled.h4`
  margin: 12px 0;
  text-align: center;  
`;

export const LinkStyle = {
  color: 'currentColor'  
}