import React from 'react';
import {Author, LinkStyle} from './styles';
import { Link } from 'react-router-dom';

export default function BookAuthor(props) {
  return (
    <Author>
       <Link to="" style={LinkStyle}>
         {props.author}
       </Link>
    </Author> 
  )
}