import React from 'react';
import { FavoriteWrapper } from './styles';

function Favorites(props) {
  return (
     <FavoriteWrapper>
        Избранное: {props.counter}
     </FavoriteWrapper>
  )
}

export default Favorites;