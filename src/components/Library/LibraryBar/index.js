import React from 'react';
import {BarWrapper} from './styles';
import SearchBook from './SearchBook';
import Favorites from './Favorites';

function LibraryBar(props) {
  return (
     <BarWrapper>
       <SearchBook/>
       <Favorites counter={props.counter}/>
     </BarWrapper>
  )
}

export default LibraryBar;