import styled from 'styled-components';

export const BarWrapper = styled.aside`
  background-color: #fff;
  padding: 32px;
  box-shadow: inset 0 0 0 1px rgba(0,0,0,.03), 8px 14px 38px rgba(39,44,49,.06), 1px 3px 8px rgba(39,44,49,.03);
  max-height: 400px;
  position: sticky;
  top: 16px;
  @media (max-width: 480px) {
    position: relative;
  }
`;