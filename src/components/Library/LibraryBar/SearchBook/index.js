import React from 'react'
import {Field, reduxForm} from 'redux-form';
import TextField from 'material-ui/TextField';

const renderTextField = ({input, label}) => (
  <TextField
    label={label}
    style={{width: '100%'}}
    InputLabelProps={{
      shrink: true,
    }}
    placeholder="введите название"
  />
);

const SearchBook = (props) => {
  return (
    <form>
      <Field name="firstName" component={renderTextField} label="Найти книгу"/>
    </form>
  )
};

export default reduxForm({
  form: 'SearchBook'
})(SearchBook)