import React from 'react';
import {MenuItem} from 'material-ui/Menu';

const renderSelectMenuItem = (items) => {
  return [
    <MenuItem value={10}>Thirty</MenuItem>,
    <MenuItem value={20}>Thirty</MenuItem>,
    <MenuItem value={30}>Thirty</MenuItem>,
    <MenuItem value={40}>Thirty</MenuItem>
  ]
};

function SelectMenuItem(props) {
  const {menuItems} = props;
  return renderSelectMenuItem(menuItems);
}

export default SelectMenuItem;