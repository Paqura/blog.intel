import React, { Component } from 'react';
import Snackbar from 'material-ui/Snackbar';

class NoAuthError extends Component {  
  state = {
    open: Boolean(this.props.error),
    vertical: 'bottom',
    horizontal: 'right',
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {    
    const { vertical, horizontal, open} = this.state;
    const { error } = this.props;    
    return (
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={this.handleClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{error}</span>}
        />
    );
  }
};

export default NoAuthError;
