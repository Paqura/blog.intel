import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {loadingHtml} from './render';
import renderHTML from 'react-render-html';
import './style.css';

export default function DLoader() {
  const res = renderHTML(loadingHtml);
  return (
    <ReactCSSTransitionGroup
      className="stage"
      transitionName="crossfade"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnterTimeout={500}
      transitionLeaveTimeout={300}
    >
      {res}
    </ReactCSSTransitionGroup>
  )
};