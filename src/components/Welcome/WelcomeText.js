import React from 'react';
import styled from 'styled-components';
import Paper from 'material-ui/Paper';

const FlyingText = styled.span`
  position: relative;
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  background-color:#fff;
  padding: 8px 16px;
  display: inline-block;  
  @media (max-width: 480px) {
  font-size: 18px;
  }
`;

const FlyingTextWrapper = styled.h2`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

function WelcomeText(props) {
  return (
    <FlyingTextWrapper>
      <Paper>
        <FlyingText>Здесь будет много интересного,</FlyingText>
      </Paper>
      <Paper>
        <FlyingText>не пропускай</FlyingText>
      </Paper>
    </FlyingTextWrapper>
  )
}

export default WelcomeText;