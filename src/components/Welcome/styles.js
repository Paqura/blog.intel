import styled from 'styled-components';
import { grey } from 'material-ui/colors';

export const List = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(280px, 1fr));
  grid-gap: 16px;
  width: 100%;
  @media (max-width: 480px) {
     margin-top: 16px;
  }
`;

export const Item = styled.li`
  display: flex;
  flex-wrap: wrap;  
`;

export const ItemHeader = styled.h4`
   margin: 0;
   font-size: 18px;
   @media (max-width: 480px) {
     font-size: 16px;
   }
`;

export const FeatureDescription = styled.p`
   font-weight: 400;
   line-height: 1.4;
   @media (max-width: 480px) {
     font-size: 14px;
   }
`;

export const LinkStyle = {
  textDecoration: 'none',
  color: `${grey[900]}`,
  fontWeight: 'bold',
  position: 'relative'
};

export const Img = {
  width: '100%',
  height: '180px',
};
