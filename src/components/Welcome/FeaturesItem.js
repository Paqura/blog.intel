import React from 'react';
import {FeaturesListMap} from './FeaturesListMap';
import {Item, LinkStyle, Img, ItemHeader, FeatureDescription} from './styles';
import {Link} from 'react-router-dom';
import {CardMedia, CardContent} from 'material-ui/Card';
import Paper from 'material-ui/Paper';

const renderFeaturesList = (feature) => {
  return (
    <Paper key={feature.name} className='hovering'>
      <Link to={feature.path} style={LinkStyle}>
        <Item>
          <CardMedia
            style={Img}
            image={feature.img}
            title="Features Images"
          />
          <CardContent>
            <ItemHeader>
              {feature.name}
            </ItemHeader>
            <FeatureDescription>
              {feature.description}
            </FeatureDescription>
          </CardContent>
        </Item>
      </Link>
    </Paper>
  )
};

function FeaturesItem(props) {
  return FeaturesListMap.map(it => renderFeaturesList(it))
}

export default FeaturesItem;