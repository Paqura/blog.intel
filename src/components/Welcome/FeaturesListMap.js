export const FeaturesListMap = [
  {
    name: 'Библиотека',
    img: 'https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/library-min.jpg?alt=media&token=b564b3ce-ec14-40da-8927-7633fe1a46ac',
    description: 'В библиотеке будут собраны книги, которые я считаю достойными прочтения. Какие-то из них прочитаны мной, некоторые с пометкой "К чтению" выбраны для ознакомления.',
    path: '/library'
  },
  {
    name: 'Картины',
    img: 'https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/pictures-min.jpg?alt=media&token=7bdd29a4-fa3f-4ed7-8699-64e6db9f1b76',
    description: 'Многие понравившиеся мне художники различных эпох, пишущие в различной технике, жанре, собраны под одним куполом.',
    path: '/pictires'
  },
  {
    name: 'Кино',
    img: 'https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinema-min.jpg?alt=media&token=8e05018b-b759-4fc0-973f-6699d4707312',
    description: 'Сюда я буду добавлять фильмы, которые повлияли на меня, или показались значимыми. Смесь жанров, направлений. Естественно, советую их остальным.',
    path: '/cinema'
  },
  {
    name: 'Стихотворения',
    img: 'https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/poems.jpg?alt=media&token=80d9de7d-37fd-4f47-9172-2ca80e873290',
    description: 'На Земле жило много прекрасных поэтов, которых обошли стороной. Здесь им нашлось место.',
    path: '/poems'
  }
];