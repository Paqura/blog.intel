import React from 'react';
import {List} from './styles';
import FeaturesItem from './FeaturesItem';

function FeaturesList(props) {
  return (
     <List>
        <FeaturesItem/>
     </List>
  )
}

export default FeaturesList;