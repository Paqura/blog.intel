import React, {Component} from 'react';
import { connect } from 'react-redux';
import LibraryList from '../Library';
import LibraryBar from '../Library/LibraryBar';
import {fetchLibrary, addBookMark} from '../../ducks/library/actions';
import {getLibrary} from '../../ducks/library/selectors';
import styled from 'styled-components';
import { moduleName } from '../../ducks/library';
import Loader from '../../components/Loader';

const LibraryGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-gap:32px;
  max-width: 1280px;
  margin: 0 auto;
  padding: 16px;
  @media (max-width: 480px) {
    grid-template-columns: 1fr;
     padding: 4px;
  }
`;

class Library extends Component {
  componentDidMount() {
    this.props.fetchLibrary()
  }


  render() {
    const { library, loading, addBookMark, bookmarks } = this.props;
    const bookmarksCounter = bookmarks.length;
    return (
      loading 
        ?
        <Loader />
        :
        <LibraryGrid>
        <LibraryBar counter={bookmarksCounter}/>
        <LibraryList library={library || []} addBookMark={addBookMark} bookmarks={bookmarks}/>
      </LibraryGrid>      
     
    )
  }
}

const mapStateToProps = state => ({
   library: getLibrary(state),
   loading: state[moduleName].get('loading'),
   bookmarks: state[moduleName].get('bookmarks')
});

const mapDispatchToProps = ({
  fetchLibrary,
  addBookMark
});

export default connect(mapStateToProps, mapDispatchToProps)(Library);