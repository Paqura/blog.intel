import React, { Component } from 'react';
import ProfilePage from '../Profile';

class Profile extends Component {
  render() {
    return (
      <main>
         <ProfilePage/>
      </main>
    )
  }
}

export default Profile;