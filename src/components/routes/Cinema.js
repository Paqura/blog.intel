//@flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import CinemaList from '../Cinemas/CinemaList';
import {fetchCinemas, changeSawCinema, addRating} from '../../ducks/cinemas/actions';
import {stateSelector, finishedCinemasID} from '../../ducks/cinemas/selectors';
import { moduleName } from '../../ducks/cinemas/index';

type CinemaObj = {
  title: string,
  img: string,
  text: string,
  genre: string,
  type: string,
  rating: string,
  url: string,
  status: string,
  uid: string
};

type Props = {
   cinemas: Array<CinemaObj>,
   loading: boolean,
   fetchCinemas: Function,
   changeSawCinema: Function,
   finishedCinemas: Function,
   addRating: Function,
   status: string,
   rating?: boolean   
};

class Cinema extends Component<Props> {
  componentDidMount() {
    this.props.fetchCinemas();    
  }

  shouldComponentUpdate(nextProps) {
    return  this.props.cinemas !== nextProps.cinemas || 
    this.props.loading !== nextProps.loading ||
    this.props.ratingVisible !== nextProps.ratingVisible ||
    this.props.rating !== nextProps.rating;
  }

  render() {    
    return (
      <main>
        <CinemaList {...this.props}/>
      </main>
    )
  }
}

const mapStateToProps = state => ({
  cinemas: stateSelector(state),   
  loading: state[moduleName].get('loading'),
  finishedCinemas: finishedCinemasID(state),
  rating: state[moduleName].get('ratingID'),
  ratingVisible: state[moduleName].get('ratingVisible'),
  currentCinema: state[moduleName].get('currentCinema') || {}
});

const mapDispatchToProps = ({
  fetchCinemas,
  changeSawCinema,
  addRating
});

export default connect(mapStateToProps, mapDispatchToProps)(Cinema);