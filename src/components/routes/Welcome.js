import React, { Component } from 'react';
import WelcomeText from '../Welcome/WelcomeText';
import FeaturesList from '../Welcome/FeaturesList';

class Welcome extends Component {
  render() {
    return (
      <main className='welcome-page'>
        <WelcomeText/>
        <FeaturesList/>
      </main>
    )
  }
}

export default Welcome;