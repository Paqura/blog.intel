//@flow

import React from 'react';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';

const styles = theme => ({
  button: {
    margin: 0
  },
  input: {
    display: 'none',
  },
});


function ChangeAvatarButton (props) {
  const { classes } = props;
  return (
      <Button variant="raised" color="primary" className={classes.button}>
        Изменить аватар
      </Button>   
  );
}

export default withStyles(styles)(ChangeAvatarButton);