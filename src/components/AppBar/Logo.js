import React from 'react';
import renderHTML from "react-render-html";
import {LogoSvg} from './LogoSvg';
import styled from 'styled-components';

const LogoWrapper = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0;
  @media (max-width: 480px) {   
    display: none;
  }
`;

function Logo(props) {
  return (
    <LogoWrapper>
       {renderHTML(LogoSvg)}
    </LogoWrapper>
  )
}

export default Logo;