import styled from 'styled-components';
import {blueGrey} from 'material-ui/colors';

export const BarWrapper = styled.header`
  position: sticky;
  top: 0;
  left: 0;
  height: auto;
  background-color: ${blueGrey[800]};
  @media (max-width: 480px) {
     overflow-x: scroll;  
  }  
`;

export const BarInner = styled.nav`
   max-width: 1280px;
   height: auto;
   margin: 0 auto;
   padding: 8px 16px;
   display: flex;   
   @media (max-width: 480px) {
     padding: 0; 
  }  
`;

export const NavBarLinkWrapper = styled.ul`
   display: flex;
   flex-grow: 2;
   padding: 0;
   margin: 0 0 0 32px;
   @media (max-width: 480px) {
     margin-left: 16px; 
  } 
`;

export const AuthList = styled.ul`
  list-style-type: none;
  display: flex;
  align-items: center;
  padding: 0;
  margin: 0;
  @media (max-width: 480px) {
     position:sticky;
    left: 0;
    top: 0;
    z-index: 99;
    order: -1;
    width: 100%;
    padding: 8px 8px;
    background-color:#454c67;
    min-width: 120px;
  }
`;

export const AuthItem = styled.span`
  list-style-type: none;
  display: flex;
  align-items: center;
  padding: 0;
  margin: 0;
  color: #fff;
  cursor: pointer;
  @media (max-width: 480px) {
    position:sticky;
    left: 0;
    top: 0;
    z-index: 99;
    order: -1;
    width: 100%;
    padding: 8px 8px;
    background-color:#454c67;
    
  }
`;

export const UserName = styled.span`
  cursor: pointer;
  color: #fff;
  text-decoration: none;
  margin-left: 12px;
`;

export const UserProfileLink = {
  display: 'flex',
  alignItems: 'center',
  textDecoration: 'none',

};