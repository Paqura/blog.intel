export const navBarLinks = [
  {
    path: '/profile',
    name: 'Профиль'
  },
  {
    path: '/library',
    name: 'Библиотека'
  },
  {
    path: '/pictures',
    name: 'Картины'
  },
  {
    path: '/cinema',
    name: 'Кино'
  },
  {
    path: '/poems',
    name: 'Стихотворения'
  }
];