import React from 'react';
import {AuthList, AuthItem, UserName, UserProfileLink} from './styles';
import {Link} from 'react-router-dom';
import {userLogo} from './user';
import renderHTML from 'react-render-html';
import Fingerprint from 'material-ui-icons/Fingerprint';

function Auth(props) {
  const {userName} = props;
  return (
    <AuthList>
      {userName ?
        <Link to="/profile" style={UserProfileLink}>
          {renderHTML(userLogo)}          
          <UserName>
            {userName}
          </UserName>
        </Link>
        : <AuthItem
          onClick={props.showAuthPopup}
        >
          <Fingerprint style={{marginRight: '8px'}}/>
          Войти
        </AuthItem>
      }
    </AuthList>
  )
}

export default Auth;