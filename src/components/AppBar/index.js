import React, {Component} from 'react';
import Paper from 'material-ui/Paper';
import { connect } from 'react-redux';
import Auth from './Auth';
import NavBarLinks from './NavBarLinks';
import Logo from './Logo';
import {BarWrapper, BarInner} from './styles';
import {showAuthPopup} from "../../ducks/auth/actions";

class  ApplicationBar extends Component{
  render() {
    return (
      <Paper>
        <BarWrapper>
          <BarInner>
            <Logo/>
            <NavBarLinks/>
            <Auth
              showAuthPopup={this.props.showAuthPopup}
              userName={this.props.userName}
            />
          </BarInner>
        </BarWrapper>
      </Paper>
    );
  }
}


const mapDispatchToProps = ({
  showAuthPopup
});


export default connect(null, mapDispatchToProps, null, {pure: false})(ApplicationBar);