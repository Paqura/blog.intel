import React from 'react';
import {navBarLinks} from "./Links";
import {NavBarLinkWrapper} from './styles';
import {NavLink} from 'react-router-dom';

const renderNavBarLinks = link => {
  return (
    <NavLink
      exact
      to={link.path}
      className='nav-link'
      key={link.name}
    >
      {link.name}
    </NavLink>
  )
};

function NavBarLinks(props) {
  return (
    <NavBarLinkWrapper>
      {navBarLinks.map(it => renderNavBarLinks(it))}
    </NavBarLinkWrapper>
  )
}

export default NavBarLinks;