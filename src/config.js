import firebase from 'firebase';

export const appName = "blog-25efd";

export const firebaseConfig = {
  apiKey: "AIzaSyAbf0VUOyhxrGuBiZaNFxJvIyljTGx-CTo",
  authDomain: "blog-25efd.firebaseapp.com",
  databaseURL: "https://blog-25efd.firebaseio.com",
  projectId: "blog-25efd",
  storageBucket: "blog-25efd.appspot.com",
  messagingSenderId: "199922252216"
};

firebase.initializeApp(firebaseConfig);