import { cinemas } from './cinema';
import { library } from './library';
import firebase from 'firebase';

export function saveEventsToFB() {
  const eventsRef = firebase.database().ref('/cinemas')
  cinemas.forEach(cinema => eventsRef.push(cinema))
}

window.runMigrationCinemas = function () {
  firebase.database().ref('/cinemas').once('value', data => {
    if (!data.val()) saveEventsToFB()
  })
};

export function saveBooksToFB() {
  const eventsRef = firebase.database().ref('/library')
  library.forEach(book => eventsRef.push(book))
}

window.runMigrationBooks = function () {
  firebase.database().ref('/library').once('value', data => {
    if (!data.val()) saveBooksToFB()
  })
};

