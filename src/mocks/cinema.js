export const cinemas = [
  {
    "title": "Террор",
    "text": "В 1845 году экспедиция под командованием опытного полярного исследователя сэра Джона Франклина отправляется на судах «Террор» и «Эребус» к северному побережью Канады на поиск северо-западного прохода из Атлантического океана в Тихий — и бесследно исчезает. Поиски ее затянулись на несколько десятилетий, сведения о ее судьбе собирались буквально по крупицам, и до сих пор картина происшедшего пестрит белыми пятнами.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fterror.jpg?alt=media&token=7515a41b-c0fa-41ae-af15-8e9ada5a0725",
    "genre": "ужасы драма",
    "type": "сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/terror-2018-738318/",
    "link": "http://hdrezka.name/series/drama/27297-terror-2018.html"
  },
  {
    "title": "Секретное досье",
    "text": "История Кэтрин Грэм, первой женщины-издателя газеты «Вашингтон пост», и редактора Бена Брэдли. Они вступают в гонку с «Нью-Йорк таймс» за право пролить свет на государственные тайны, скрывавшиеся более 30 лет. Журналистам придется преодолеть свои разногласия и рискнуть карьерой и свободой, чтобы мир узнал правду.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fsecret_docie.jpg?alt=media&token=03f03394-a647-4feb-b96c-4dc1152c441f",
    "genre": "биография триллер драма",
    "type": "кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/sekretnoe-dose-2017-1040567/",
    "link": "http://hdrezka.name/films/drama/26943-sekretnoe-dose-2017.html"
  },
  {
    "title": "Ветреная река",
    "text": " пустыне на территории индейской резервации «Ветреная река» егерь Кори Ламберт находит изувеченное тело молодой девушки. Опытный охотник с трагическим прошлым, он берется помочь в расследовании этого дела агенту ФБР Джейн Бэннер, которая оказывается в мире, полном загадок и насилия. Но смогут ли они противостоять зловещим силам этих мест, где сама природа пропитана горечью и жаждой мщения?",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fwindy_river.jpg?alt=media&token=166a7508-0b8a-46a7-9b5f-3b3134e46e9d",
    "genre": "триллер драма",
    "type": "кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/vetrenaya-reka-2016-958442/",
    "link": "http://hdrezka.name/films/action/25380-vetrenaya-reka-2017.html"
  },
  {
    "title": "Сыны анархии",
    "text": "Сериал рассказывает о клубе байкеров, которые пытаются защитить свой городок от наркотиков и влияния извне, хотя сами грешат торговлей оружием. Молодой лидер клуба не слишком уверен в том, что выбрал правильный путь, и пытается что-то изменить, к неудовольствию своих мамы и отчима.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fsons_of_anarchy.jpg?alt=media&token=4223eac3-9106-4d19-8195-66753c05c3d5",
    "genre": "драма",
    "type": "сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/syny-anarkhii-2008-417846/",
    "link": "http://hdrezka.name/series/thriller/1732-syny-anarhii-2008.html"
  },
  {
    "title": "Оставленные",
    "text": "Все начинается с того, что весь мир содрогается от странного и пугающего происшествия, которое так ни кто и не смог объяснить. В один из самых обычных дней загадочным образом исчезает около двух процентов населения планеты, а это в свою очередь сотни миллионов людей. Не исключением стал и небольшой городок Мейплтон, расположенный в пригороде Нью-Йорка. Это происшествие навсегда изменило жизнь горожан, ведь в тот злополучный день, многие потеряли родных и близких. Каждый из них имеет свое мнение по поводу произошедшего: кот-то считает это вознесением, а кто-то предзнаменованием чего-то грандиозного. Но что же произошло на самом деле?",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fleftovers.jpg?alt=media&token=2df7e6f5-462b-49e8-8a64-b12f481aacb8",
    "genre": "драма фэнтези детектив",
    "type": "сериал",
    "rating": "8",
    "url": "https://www.kinopoisk.ru/film/ostavlennye-2014-737589/",
    "link": "http://hdrezka.name/series/fantasy/1832-ostavlennye-2014.html"
  },
  {
    "title": "Охота на Унабомбера",
    "text": "История противостояния офицера ФБР Джима Фицджеральда и опасного преступника Теодора Качински по прозвищу Унабомбер, рассылавшего по почте самодельные бомбы.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Funabomber.jpg?alt=media&token=f28123ca-1124-4afe-82b0-7057bb074cb3",
    "genre": "драма биография",
    "type": "сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/okhota-na-unabombera-2017-1009475/",
    "link": "http://hdrezka.name/series/drama/25340-ohota-na-unabombera.html"
  },
  {
    "title": "Ошибки прошлого",
    "text": "Даниэлю Холдену приходится заново строить свою жизнь, после 19 лет, проведенных в камере смертников…",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Frectify.jpg?alt=media&token=f42001ea-172b-4bcd-b37b-0af9be8302e9",
    "genre": "драма биография",
    "type": "сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/oshibki-proshlogo-2013-745562/",
    "link": "http://hdrezka.name/series/drama/1922-oshibki-proshlogo-ispravlyat-oshibki-isprav.html"
  },
  {
    "title": "Кровавый алмаз",
    "text": "Действие разворачивается на фоне тотального хаоса в период гражданской войны 1999-го года в Сьера-Леоне. Ловкий и отъявленный контрабандист по имени Дэнни Арчер промышляет торговлей алмазами абсолютно не беспокоясь о том, что эти алмазы используются террористами для приобретения оружия с целью дальнейшего разжигания войны. В вихре всех этих событий Арчер знакомится с обычным рыбаком из городка Манд, Соломоном Венди и узнаёт от него о необычном, очень редком розовом алмазе. Будучи разлучённым со своей семьёй и отправленным на рудники для добывания алмазов, Соломон неожиданно нашёл там этот редкий самородок и спрятал его, чтобы потом его продать. Понимая какие возможности открывает владение этой драгоценностью, Арчер соглашается помочь рыбаку найти и спасти сына, которого забрали для того, чтобы сделать его солдатом так называемой детской армии. Это странное, совместное путешествие, через охваченную пламенем войны страну, цель которого спасение сына рыбака, в какой-то момент полностью меняет мировозрение контрабандиста...",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fblood_stone.jpg?alt=media&token=9d34a25e-48a3-48be-b42b-291fbea73573",
    "genre": "триллер драма",
    "type": "кино",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/krovavyy-almaz-2006-102376/",
    "link": "http://hdrezka.name/films/thriller/4366-krovavyy-almaz-2006.html"
  },
  {
    "title": "Стражи галактики 2",
    "text": "",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fstraji_2.jpg?alt=media&token=0271f9d7-ae1c-48e6-b88f-9e37075ba8ea",
    "genre": "приключения фантастика",
    "type": "мультфильм",
    "rating": "8",
    "url": "https://www.kinopoisk.ru/film/strazhi-galaktiki-chast-2-2017-841263/",
    "link": "http://hdrezka.name/films/fiction/22867-strazhi-galaktiki-2-2017.html"
  },
  {
    "title": "Легенды осени",
    "text": "",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Flegeng_autumn.jpg?alt=media&token=eba1e471-8cbc-4ee7-977b-0416d39b6deb",
    "genre": "драма военный",
    "type": "кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/legendy-oseni-1994-8827/",
    "link": "http://hdrezka.name/films/drama/5045-legendy-oseni-1994.html"
  },
  {
    "title": "Во всем виноват енот",
    "text": "",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1%2Fenot.jpg?alt=media&token=23f8d930-7928-4e4b-adf3-29a54da68140",
    "genre": "драма",
    "type": "кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/vo-vsem-vinovat-enot-2016-945153/",
    "link": "http://hdrezka.name/films/drama/24947-vo-vsem-vinovat-enot.html"
  },
  {
    "title": "Ганнибал: Восхождение",
    "text": "Первая глава жизни самого знаменитого и хитроумного маньяка всех времен… Ему было шесть, когда на его глазах жестоко убили его семью. В двадцать, бежав во Францию, а затем в США, он получил блестящее образование психиатра. Впереди его ждала жизнь, полная привилегий. Но жажда мести за убийство семьи только разгоралась в нем…",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fhannibal-min.jpg?alt=media&token=15334a43-8975-4937-a82e-89c4992f86dd",
    "genre": "Триллер",
    "type": "Кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/gannibal-voskhozhdenie-2006-50948/",
    "link": "http://hdrezka.name/films/foreign/4207-gannibal-voshozhdenie.html"
  },
  {
    "title": "Ледяной",
    "text": "Криминальный байопик проливает свет на историю о наемном убийце Ричарде Куклински, сумевшем долгое время параллельно вести две абсолютно взаимоисключающие жизни: с одной стороны он жестокий киллер, с другой — образцовый семьянин.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fcolder-min.jpg?alt=media&token=f347c7b0-6b6f-4fd6-b4a6-9b3137ea13a7",
    "genre": "Боевик",
    "type": "Кино",
    "rating": "5",
    "url": "https://www.kinopoisk.ru/film/ledyanoy-2012-570051/",
    "link": "http://hdrezka.name/films/biographical/413-ledyanoy-2012.html"
  },
  {
    "title": "Острые козырьки",
    "text": "Британский сериал о криминальном мире Бирмингема 20-х годов прошлого века, в котором многолюдная семья Шелби стала одной из самых жестоких и влиятельных гангстерских банд послевоенного времени. Фирменным знаком группировки, промышлявшей грабежами и азартными играми, стали зашитые в козырьки лезвия.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fpeaky-min.jpg?alt=media&token=2da50c86-5066-43a3-b3a5-4ca65735c444",
    "genre": "Драма",
    "type": "Сериал",
    "rating": "9",
    "url": "https://www.kinopoisk.ru/film/ostrye-kozyrki-2013-716587/",
    "link": "http://hdrezka.name/series/drama/1929-ostrye-kozyrki-2013.html"
  },
  {
    "title": "Живое",
    "text": "Группа исследователей с международного космического корабля обнаруживает жизнь на Марсе. Они еще не подозревают, какие события повлечет за собой их открытие.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Falive-min.jpg?alt=media&token=b0c9c9b8-e756-4399-b103-4bef5e9bbf2c",
    "genre": "Ужасы",
    "type": "Кино",
    "rating": "5",
    "url": "https://www.kinopoisk.ru/film/zhivoe-2017-966995/",
    "link": "http://hdrezka.name/films/horror/23631-zhivoe-2017.html"
  },
  {
    "title": "Клетка",
    "text": "Главная героиня фильма, психиатр, проникает в сознание серийного убийцы, находящегося в коме. На его счету уже почти десяток жертв. Ими, как правило, становятся молодые привлекательные девушки, которых маньяк истязает самым нечеловеческим образом, прежде чем лишить жизни.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fcell-min.jpg?alt=media&token=659615c4-c26b-48d0-a64e-ec0436d0b32d",
    "genre": "Ужасы триллер",
    "type": "Кино",
    "rating": "5",
    "url": "https://www.kinopoisk.ru/film/kletka-2000-630/",
    "link": "http://hdrezka.name/films/foreign/3583-kletka.html"
  },
  {
    "title": "Форма воды",
    "text": "Действие разворачивается в начале 60-х прошлого века. Главная героиня — немая уборщица в научной лаборатории. В стенах секретного учреждения идёт работа по изучению отловленного человека-амфибии. Женщина влюбляется в мутанта и помогает ему сбежать.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fshape_of_water-min.jpg?alt=media&token=8d316c46-5932-4629-8628-99e0b50b4e71",
    "genre": "Драма фантастика",
    "type": "Кино",
    "rating": "4",
    "url": "https://www.kinopoisk.ru/film/forma-vody-2017-977743/",
    "link": "http://hdrezka.name/films/drama/26419-forma-vody-2017.html"
  },
  {
    "title": "Бронзовый сад",
    "text": "Жизнь Фабиана и Лилы Данубио круто меняется, когда их четырёхлетняя дочь бесследно исчезает в метро Буэнос-Айреса. Нет никаких мотивов, никаких улик и свидетелей. Родители ведут отчаянные поиски на протяжении многих лет и, наконец, обнаруживают удивительное.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fbronze_garden-min.jpg?alt=media&token=11e7729b-4c51-46f1-96b8-867499886777",
    "genre": "Драма детектив",
    "type": "Сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/bronzovyy-sad-2016-1047991/",
    "link": "http://hdrezka.name/series/detective/25915-bronzovyy-sad-2017.html"
  },
  {
    "title": "Аутсайдер",
    "text": "Япония после Второй мировой войны. Американский военнопленный Ник вынимает из петли повешенного якудза Киёси. С его помощью якудза в конце-концов выбирается из тюрьмы и позже, используя связи, освобождает Ника. Поскольку Киёси считает, что должен американцу, то даёт ему работу, конечно же, связанную с грязным делами японских ОПГ. Гайдзин неплохо проявляет себя на этом поприще, чем вызывает уважение главы клана, и его официально принимают в якудза.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Foutsider.jpg?alt=media&token=f2ff5a46-9294-4390-9957-255ec915cf87",
    "genre": "Драма",
    "type": "Кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/autsayder-2018-603351/",
    "link": "http://hdrezka.name/films/drama/27276-autsayder-2018.html"
  },
  {
    "title": "Место встречи",
    "text": "",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fmesto-min.jpg?alt=media&token=6034ccc9-50b7-4be5-99e3-322c34218c19",
    "genre": "драма фантастика",
    "type": "Кино",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/mesto-vstrechi-2017-1059224/",
    "link": "http://hdrezka.name/films/drama/27324-mesto-vstrechi-2017.html"
  },
  {
    "title": "Глубина",
    "text": "Разгар Второй мировой войны. Американская подводная лодка «Тигровая акула», посланная на выручку оставшимся в живых членам экипажа британского плавучего госпиталя, вынуждена скрываться на глубине от шныряющих ближе к поверхности немецких субмарин. Уходя ближе ко дну, подводники обнаруживают, что там, внизу, есть еще что-то — явно не нацистские судна, нечто необъяснимое и, возможно, сверхъестественное.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fdeep-min.jpg?alt=media&token=22c60dfa-2708-40a7-a075-aa3b21d3df09",
    "genre": "ужасы триллер",
    "type": "Кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/glubina-2002-13148/",
    "link": "http://hdrezka.name/films/foreign/6106-glubina.html"
  },
  {
    "title": "Три билборда на границе Эббинга, Миссури",
    "text": "Спустя несколько месяцев после убийства дочери Милдред Хейс преступники так и не найдены. Отчаявшаяся женщина решается на смелый шаг, арендуя на въезде в город три билборда с посланием к авторитетному главе полиции Уильяму Уиллоуби. Когда в ситуацию оказывается втянут ещё и заместитель шерифа, инфантильный маменькин сынок со склонностью к насилию, офицер Диксон, борьба между Милдред и властями города только усугубляется.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F3-min.jpg?alt=media&token=fc5830f4-2579-4a8e-aac5-3dbdc314fb92",
    "genre": "драма",
    "type": "Кино",
    "rating": "8",
    "url": "https://www.kinopoisk.ru/film/tri-bilborda-na-granitse-ebbinga-missuri-2017-944098/",
    "link": "http://hdrezka.name/films/drama/26927-tri-bilborda-na-granice-ebbinga-missuri-2017.html"
  },
  {
    "title": "Девушка в тумане",
    "text": "Холодной зимней ночью психиатра Аугусто Флореса срочно вызывают на работу, чтобы обследовать попавшего в аварию инспектора Фогеля. Инспектор ничего не помнит, на месте аварии ничто не указывает на ее причину и на ее жертв, однако рубашка Фогеля в пятнах свежей крови, и эта кровь не его. В ходе беседы начинает восстанавливаться цепь невероятных событий, начавшихся месяц назад.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fgirl-min.jpg?alt=media&token=25c38696-ba62-4a10-b748-113bafef4804",
    "genre": "триллер криминал",
    "type": "Кино",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/devushka-v-tumane-2017-1047956/",
    "link": "http://hdrezka.name/films/thriller/26877-devushka-v-tumane-2017.html"
  },
  {
    "title": "Матрица времени",
    "text": "Саманта — крутая девчонка, которой всегда и во всем везло. Но в тот день, в пятницу, 12 февраля, что-то пошло не так, а потом та авария на трассе… Саманта оказалась в заколдованном круге проклятого дня, и теперь она вынуждена проживать ужас той пятницы снова и снова. Чтобы распутать петлю времени, она должна вычислить ошибку и исправить неверный шаг. Но каждый раз что-то не срабатывает…",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fmatrix-min.jpg?alt=media&token=5cb441a3-53ad-42a6-913b-e569e3bc7468",
    "genre": "драма фантастика",
    "type": "Кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/matritsa-vremeni-2017-556944/",
    "link": "http://hdrezka.name/films/thriller/23359-matrica-vremeni-2017.html"
  },
  {
    "title": "Убийство в Восточном экспрессе",
    "text": "Путешествие на одном из самых роскошных поездов Европы неожиданно превращается в одну из самых стильных и захватывающих загадок в истории. Фильм рассказывает историю тринадцати пассажиров поезда, каждый из которых находится под подозрением. И только сыщик должен как можно быстрее разгадать головоломку, прежде чем преступник нанесет новый удар.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fexpress-min.jpg?alt=media&token=a64ab164-d5a2-437d-9bb3-00f8df198c22",
    "genre": "детектив драма",
    "type": "Кино",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/ubiystvo-v-vostochnom-ekspresse-2017-817969/",
    "link": "http://hdrezka.name/films/detective/25699-ubiystvo-v-vostochnom-ekspresse-2017.html"
  },
  {
    "title": "Виселица",
    "text": "Журналистка Кристи Дэвис пишет статью о работе полиции, сотрудничая с детективом Руни из отдела убийств. И в первый же день они оказываются на месте преступления — девушка повешена, а на груди у нее вырезана буква. А неподалеку в здании детектив Руни находит на стене игру в виселицу и вырезанные цифры, которые совпадают с номерами его жетона и полицейского в отставке, бывшего детектива Арчера. Пенсионер Арчер с охотой берется помогать в расследовании, когда становится ясно, что в городе орудует серийный убийца — на следующий день детективы находят ещё одного повешенного.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Fhangman-min.jpg?alt=media&token=774ddb63-c113-4da8-82e1-d17747c94bc1",
    "genre": "детектив криминал",
    "type": "Кино",
    "rating": "6",
    "url": "https://www.kinopoisk.ru/film/viselitsa-2017-775410/",
    "link": "http://hdrezka.name/films/thriller/26499-viselica-2017.html"
  },
  {
    "title": "Амнезия",
    "text": "Бесследно исчезает агент ФБР Эмили Бирн, которая расследует дело одного из самых известных серийных убийц Бостона. Шесть лет спустя её находят в заброшенном доме. Эмили почти ничего не помнит о времени, проведённом в заточении. Героиня ищет разгадку тайны своего похищения и пытается снова соединиться со своей семьей, у которой теперь новая жизнь.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2Famnesia-min.jpg?alt=media&token=be54052c-fd7b-445f-b1fa-5b47734a977b",
    "genre": "триллер драма",
    "type": "Сериал",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/amneziya-2017-1046029/",
    "link": "http://hdrezka.name/series/thriller/25754-amneziya-2017.html"
  },
  {
    "title": "1922",
    "text": "История о фермере Уилфреде Джеймсе, который с помощью сына-подростка убил свою жену и бросил её тело на растерзание крысам. С тех пор крысы-трупоеды следуют за Уилфредом по пятам, ломая ему жизнь и доводя до безумия.",
    "img": "https://firebasestorage.googleapis.com/v0/b/blog-25efd.appspot.com/o/cinemas%2F1922-min.jpg?alt=media&token=0aabc0ae-3345-4242-a344-b7cd4280dc45",
    "genre": "ужасы триллер",
    "type": "Кино",
    "rating": "7",
    "url": "https://www.kinopoisk.ru/film/1922-2017-1008798/",
    "link": "http://hdrezka.name/films/drama/25891-1922-2017.html"
  }
]